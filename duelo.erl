-module(duelo).
-export([run/0]).
-compile([native]).

-define(SCREENWIDTH, 640).
-define(SCREENHEIGHT, 480).
-define(BULLETSPEED, 16).
-define(LBULLETPOS, 110).
-define(RBULLETPOS, 510).
-define(BULLETWIDTH, 20).
-define(BULLETHEIGHT, 5).
-define(LSHIELDPOS, 90).
-define(RSHIELDPOS, 540).
-define(SHIELDWIDTH, 10).
-define(DELTASCORE, 10).
-define(SCOREHEIGHT, 10).
-define(SDLK_LSHIFT, 1073742049).
-define(SDLK_RSHIFT, 1073742053).
-define(SDLK_LCTRL, 1073742048).
-define(SDLK_RCTRL, 1073742052).

run() ->
	spawn(fun init/0).

init() ->
	ok = sdl:start([video]),
	ok = sdl:stop_on_exit(),
	{ok, Window} = sdl_window:create(<<"Duelo">>, centered, centered, ?SCREENWIDTH, ?SCREENHEIGHT, []),
	{ok, Renderer} = sdl_renderer:create(Window, -1, [accelerated, present_vsync]),
	Scores = [0, 0],
	Stances = [[{bullets,291,[]}, head, body], [{bullets,284,[]}, head, body]],
	game_loop(#{window=>Window, renderer=>Renderer, stances=>Stances, scores=>Scores}).


game_loop(State) ->
	EventState = events_loop(State),
	NewState = check_colisions(EventState),
	render(NewState),
	game_loop(NewState).


events_loop(State) ->
	case sdl_events:poll() of
		false ->
			State;
		#{type:=quit} ->
			terminate();

		#{type:=key_down, repeat:=false, sym:=Key} -> 
			%io:format("key = ~p~n", [Key]),
			#{stances:=[Left, Right]} = State,
			EventState = case Key of
				?SDLK_LSHIFT -> State#{stances:=[attack(Left,?LBULLETPOS-?BULLETSPEED), Right]};
				?SDLK_LCTRL  -> State#{stances:=[defend(Left), Right]};
				?SDLK_RSHIFT -> State#{stances:=[Left, attack(Right,?RBULLETPOS+?BULLETSPEED)]};
				?SDLK_RCTRL  -> State#{stances:=[Left, defend(Right)]};
				_ -> State
			end,
			%io:format("key down!~n"),
			events_loop(EventState);

		#{type:=key_up, repeat:=false, sym:=Key} -> 
			#{stances:=[Left, Right]} = State,
			EventState = case Key of
				?SDLK_LSHIFT -> State#{stances:=[lists:delete(weapon,Left), Right]};
				?SDLK_LCTRL  -> State#{stances:=[lists:delete(shield,Left), Right]};
				?SDLK_RSHIFT -> State#{stances:=[Left, lists:delete(weapon,Right)]};
				?SDLK_RCTRL  -> State#{stances:=[Left, lists:delete(shield,Right)]};
				_ -> State
			end,
			%io:format("key up!~n"),
			events_loop(EventState);

		_ -> events_loop(State)
	end.


attack(Stance, BulletPos) ->
	case lists:member(shield, Stance) of
		true -> Stance;
		false ->
			[{bullets,Y,BulletList} | NoBulletStance] = Stance,
			%io:format("bullets = ~p~n", [BulletList]),
			[{bullets,Y,[BulletPos|BulletList]}, weapon | NoBulletStance]
	end.


defend(Stance) ->
	case lists:member(weapon, Stance) of
		true -> Stance;
		false ->
			[{bullets,Y,BulletList} | NoBulletStance] = Stance,
			[{bullets,Y,BulletList}, shield | NoBulletStance]
	end.


-define(BLOCKED_BY_RSHIELD(X, RightStance), (X+?BULLETWIDTH > ?RSHIELDPOS andalso X+?BULLETWIDTH-?BULLETSPEED =< ?RSHIELDPOS andalso lists:member(shield, RightStance))).
-define(BLOCKED_BY_LSHIELD(X, LeftStance), (X < ?LSHIELDPOS+?SHIELDWIDTH andalso X+?BULLETSPEED >= ?LSHIELDPOS+?SHIELDWIDTH andalso lists:member(shield, LeftStance))).

check_colisions(State) ->
	#{stances:=[[{bullets,Ly,LeftBullets}|LeftStance], [{bullets,Ry,RightBullets}|RightStance]], scores:=[LeftScore,RightScore]} = State,

	NewLeftScore = lists:foldl(fun(X, Acc) when X > ?SCREENWIDTH -> Acc+1; (_, Acc) -> Acc end, LeftScore, LeftBullets),
	NewRightScore = lists:foldl(fun(X, Acc) when X < 0 -> Acc+1; (_, Acc) -> Acc end, RightScore, RightBullets),
	%io:format("L: ~p | R: ~p~n", [NewLeftScore,NewRightScore]),

	RemainingLeftBullets = [X+?BULLETSPEED || X <- LeftBullets, X < ?SCREENWIDTH, not ?BLOCKED_BY_RSHIELD(X, RightStance)],
	RemainingRightBullets = [X-?BULLETSPEED || X <- RightBullets, X > 0, not ?BLOCKED_BY_LSHIELD(X, LeftStance)],

	State#{stances:=[[{bullets,Ly,RemainingLeftBullets}|LeftStance], [{bullets,Ry,RemainingRightBullets}|RightStance]], scores:=[NewLeftScore,NewRightScore]}.


render(#{renderer:=Renderer, stances:=Stances, scores:=Scores}) ->
	Models = [
		[ {head, {30,230,40,40}}, {body, {20,280,60,80}}, {shield, {90,250,10,80}}, {weapon, {80,280,30,20}}, {curtain, {0, 480,640,240}}, {bullet, {180,291,20,5}}, {bomb, {80,200,21,21}} ],
		[ {head, {570,230,40,40}}, {body, {560,280,60,80}}, {shield, {540,250,10,80}}, {weapon, {530,280,30,20}}, {curtain, {0,-240,640,240}}, {bullet, {450,284,20,5}}, {bomb, {540,200,21,21}} ]
	],
	sdl_renderer:set_draw_color(Renderer, 0, 0, 0, 0),
	ok = sdl_renderer:clear(Renderer),
	sdl_renderer:set_draw_color(Renderer, 255, 255, 255, 255),
	draw_models(Renderer, Models, Stances),
	draw_bullets(Renderer, Stances),
	draw_scores(Renderer, Scores),
	ok = sdl_renderer:present(Renderer).


draw_models(_, [], []) -> ok;
draw_models(Renderer, [Model|ModelList], [Stance|StanceList]) ->
	[sdl_renderer:draw_rect(Renderer, X, Y, W, H) || {S, {X, Y, W, H}} <- Model, lists:member(S, Stance)],
	draw_models(Renderer, ModelList, StanceList).


draw_bullets(_, []) -> ok;
draw_bullets(Renderer, [[{bullets,Y,BulletList}|_]|StanceList]) ->
	[sdl_renderer:draw_rect(Renderer, X, Y, ?BULLETWIDTH, ?BULLETHEIGHT) || X <- BulletList],
	draw_bullets(Renderer, StanceList).


draw_scores(Renderer, [LeftScore,RightScore]) ->
	sdl_renderer:draw_rect(Renderer, 0, 80, ?DELTASCORE*LeftScore, ?SCOREHEIGHT),
	sdl_renderer:draw_rect(Renderer, ?SCREENWIDTH-?DELTASCORE*RightScore, 80-?SCOREHEIGHT+1, ?DELTASCORE*RightScore, ?SCOREHEIGHT).


terminate() ->
	init:stop(),
	exit(normal).
